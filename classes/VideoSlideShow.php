<?php

/**
 * Class to create a video slide show
 * @version 1.2
 * @author Andchir <andchir@gmail.com>
 */

namespace Andchir;

class VideoSlideShow {

    private $faceDetector;
    public $config = [];

    public function __construct($faceDetector = null, $config = [])
    {
        $this->faceDetector = $faceDetector;

        $this->config = array_merge([
            'root_path' => dirname(__DIR__),
            'face_detection' => false,
            'use_opencv' => false,
            'debug' => true
        ], $config);
    }

    /**
     * @param $positionName
     * @param $durationClip
     * @param $imageWidth
     * @param $imageHeight
     * @param $movieWidth
     * @param $movieHeight
     * @param $imageFilePath
     * @param array $motionOptions
     * @return string
     */
    public function getPositionFilterOptionValue(
        $positionName,
        $durationClip,
        $imageWidth,
        $imageHeight,
        $movieWidth,
        $movieHeight,
        $imageFilePath,
        $motionOptions = []
    )
    {
        static $index = 0;
        static $indexHorizontal = 0;
        static $indexVertical = 0;
        static $indexNormal = 0;
        static $indexFace = 0;

        $names = [
            'horizontal' => ['rightToLeft', 'leftToRight'],
            'vertical' => ['topToBottom', 'bottomToTop'],
            'normal' => [
                'zoomInCenter',
                'leftTopToRightBottom',
                'rightBottomToLeftTop',
                'leftBottomToRightTop',
                'rightTopToLeftBottom',
                'zoomOutCenter'
            ],
            'face' => ['PointZoomIn', 'PointZoomOut']
        ];

        $movieAspect = $movieWidth / $movieHeight;
        $imageAspect = $imageWidth / $imageHeight;
        $faceData = null;
        $inWidth = null;
        $inHeight = null;
        $outWidth = null;
        $outHeight = null;

        if ($positionName === 'custom'
            && (
                empty($motionOptions)
                || empty($motionOptions['from'])
                || empty($motionOptions['to'])
            )
        ) {
            $positionName = 'auto';
        }
        if ($positionName === 'auto') {
            if ($this->config['face_detection']) {
                $faceData = $this->faceDetect($imageFilePath);
                if (!empty($faceData)) {
                    $positionName = $this->getValueRounded($names['face'], $indexFace);
                    $indexFace++;
                }
            }
            if ($positionName === 'auto') {
                if ($imageAspect < 1) {
                    $positionName = $this->getValueRounded($names['vertical'], $indexHorizontal);
                    $indexHorizontal++;
                } else if ($imageAspect >= 2.5) {
                    $positionName = $this->getValueRounded($names['horizontal'], $indexVertical);
                    $indexVertical++;
                } else {
                    $positionName = $this->getValueRounded($names['normal'], $indexNormal);
                    $indexNormal++;
                }
            }
        }

        switch($positionName) {
            case 'leftToRight':
                $outHeight = $movieHeight;
                $outWidth = floor($outHeight * $imageAspect);
                $inLeft = 0;
                $outLeft = 0 - floor($outWidth - $movieWidth);
                $inTop = 0;
                $outTop = 0;
                break;
            case 'rightToLeft':
                $outHeight = $movieHeight;
                $outWidth = floor($outHeight * $imageAspect);
                $inLeft = 0 - floor($outWidth - $movieWidth);
                $outLeft = 0;
                $inTop = 0;
                $outTop = 0;
                break;
            case 'topToBottom':
                $outWidth = $movieWidth;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0;
                $outLeft = 0;
                $inTop = 0;
                $outTop = 0 - floor($outHeight - $movieHeight);
                break;
            case 'bottomToTop':
                $outWidth = $movieWidth;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0;
                $outLeft = 0;
                $inTop = 0 - floor($outHeight - $movieHeight);
                $outTop = 0;
                break;
            case 'leftTopToRightBottom':
                $outWidth = $movieWidth * 2;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0;
                $outLeft = 0 - floor($outWidth - $movieWidth);
                $inTop = 0;
                $outTop = 0 - floor($outHeight - $movieHeight);
                break;
            case 'leftBottomToRightTop':
                $outWidth = $movieWidth * 2;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0;
                $outLeft = 0 - floor($outWidth - $movieWidth);
                $inTop = 0 - floor($outHeight - $movieHeight);
                $outTop = 0;
                break;
            case 'rightBottomToLeftTop':
                $outWidth = $movieWidth * 2;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0 - floor($outWidth - $movieWidth);
                $outLeft = 0;
                $inTop = 0 - floor($outHeight - $movieHeight);
                $outTop = 0;
                break;
            case 'rightTopToLeftBottom':
                $outWidth = $movieWidth * 2;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0 - floor($outWidth - $movieWidth);
                $outLeft = 0;
                $inTop = 0;
                $outTop = 0 - floor($outHeight - $movieHeight);
                break;
            case 'zoomInCenter':
            case 'zoomOutCenter':

                if ($imageAspect < $movieAspect) {
                    $inWidth = $movieWidth;
                    $inHeight = floor($inWidth / $imageAspect);
                } else {
                    $inHeight = $movieHeight;
                    $inWidth = floor($inHeight * $imageAspect);
                }
                $inTop =  0 - floor(($inHeight / 2) - ($movieHeight / 2));
                $outWidth = $inWidth * 1.7;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0;
                $outLeft = floor(($inWidth / 2) - ($outWidth / 2));
                $outTop = floor((($movieHeight / 2) - ($outHeight / 2)) * 0.8);

                if ($positionName === 'zoomOutCenter') {
                    $this->reverseDataPositions($inLeft, $inTop, $inWidth, $inHeight, $outLeft, $outTop, $outWidth, $outHeight);
                }

                break;
            case 'PointZoomIn':
            case 'PointZoomOut':

                $pos = [
                    'width' => $faceData['w'],
                    'left' => $faceData['x'],
                    'top' => $faceData['y']
                ];
                if ($faceData['w'] / $faceData['h'] <= 1) {
                    $pos['width'] = $faceData['h'] * $movieAspect;
                    $pos['left'] = $faceData['x'] - (($pos['width'] - $faceData['w']) / 2);
                }
                if ($faceData['w'] / $faceData['h'] > 2) {
                    $pos['top'] = min(0, ($faceData['y'] - (($faceData['h'] * $movieAspect) - $faceData['h']) / 2));
                }

                $scaleRatio = $movieWidth / $pos['width'];
                $inLeft = 0;
                $inTop = 0;

                if ($imageAspect < $movieAspect) {
                    $inWidth = $movieWidth;
                    $inHeight = floor($inWidth / $imageAspect);
                    $inTop =  0 - floor(($inHeight / 2) - ($movieHeight / 2));
                } else {
                    $inHeight = $movieHeight;
                    $inWidth = floor($inHeight * $imageAspect);
                }

                $outWidth = floor($imageWidth * $scaleRatio);
                $outHeight = floor($outWidth / $imageAspect);

                $outLeft = 0 - floor($pos['left'] * ($outWidth / $imageWidth));
                $outTop = 0 - floor($pos['top'] * ($outWidth / $imageWidth));

                if ($positionName === 'PointZoomOut') {
                    $this->reverseDataPositions($inLeft, $inTop, $inWidth, $inHeight, $outLeft, $outTop, $outWidth, $outHeight);
                }

                break;
            case 'custom':

                $scale = $movieWidth / $motionOptions['from']['width'];
                $inWidth = floor($imageWidth * $scale);
                $inHeight = floor($inWidth / $imageAspect);
                $inLeft = 0 - floor($motionOptions['from']['x'] * ($inWidth / $imageWidth));
                $inTop = 0 - floor($motionOptions['from']['y'] * ($inWidth / $imageWidth));

                $scale = $movieWidth / $motionOptions['to']['width'];
                $outWidth = floor($imageWidth * $scale);
                $outHeight = floor($outWidth / $imageAspect);
                $outLeft = 0 - floor($motionOptions['to']['x'] * ($outWidth / $imageWidth));
                $outTop = 0 - floor($motionOptions['to']['y'] * ($outWidth / $imageWidth));

                break;
            default:
                $outWidth = $movieWidth * 2;
                $outHeight = floor($outWidth / $imageAspect);
                $inLeft = 0;
                $outLeft = 0;
                $inTop = 0;
                $outTop = 0;
        }
        if (is_null($inWidth)) {
            $inWidth = $outWidth;
        }
        if (is_null($inHeight)) {
            $inHeight = $outHeight;
        }
        if (is_null($outWidth)) {
            $outWidth = $inWidth;
        }
        if (is_null($outHeight)) {
            $outHeight = $inHeight;
        }

        // FRAME_NUMBER=LEFT/TOP:WIDTHxHEIGHT:OPACITY
        $transformGeometry = "0={$inLeft}/{$inTop}:{$inWidth}x{$inHeight}:100;";
        $transformGeometry .= "{$durationClip}={$outLeft}/{$outTop}:{$outWidth}x{$outHeight}:100";

        $index++;

        return $transformGeometry;
    }

    /**
     * @param $inLeft
     * @param $inTop
     * @param $inWidth
     * @param $inHeight
     * @param $outLeft
     * @param $outTop
     * @param $outWidth
     * @param $outHeight
     */
    public function reverseDataPositions(&$inLeft, &$inTop, &$inWidth, &$inHeight, &$outLeft, &$outTop, &$outWidth, &$outHeight)
    {
        list($tmpLeft, $tmlTop, $tmpWidth, $tmpHeight) = [$inLeft, $inTop, $inWidth, $inHeight];
        list($inLeft, $inTop, $inWidth, $inHeight) = [$outLeft, $outTop, $outWidth, $outHeight];
        list($outLeft, $outTop, $outWidth, $outHeight) = [$tmpLeft, $tmlTop, $tmpWidth, $tmpHeight];
    }

    /**
     * @param string $imageFilePath
     * @param float|int $minSizePercent
     * @return array
     */
    public function faceDetect($imageFilePath, $minSizePercent = 4.5)
    {
        $detectedData = [];
        $imageSize = getimagesize($imageFilePath);

        if ($this->config['use_opencv'] && function_exists('face_detect')) {

            $cascadeFilePath = $this->config['root_path'] . '/assets/opencv_data/';
            $cascadeFilePath .= 'haarcascades/haarcascade_frontalface_alt.xml';

            $detectedData = face_detect($imageFilePath, $cascadeFilePath);

        } else if ($this->faceDetector) {
            $this->faceDetector->faceDetect($imageFilePath);
            $faceData = $this->faceDetector->getFace();
            if (!empty($faceData)) {
                $faceData['h'] = $faceData['w'];
                $detectedData[] = $faceData;
            }
        }

        $detectedData = array_filter($detectedData, function ($data) use ($imageSize, $minSizePercent) {
            return ($data['w'] / $imageSize[0]) * 100 > $minSizePercent;
        });
        $detectedData = array_merge($detectedData);

        if (empty($detectedData)) {
            if ($this->config['debug']) {
                echo "{$imageFilePath} - face not detected.";
                exit;
            }
            return null;
        }

        $xMin = min(array_column($detectedData, 'x'));
        $yMin = min(array_column($detectedData, 'y'));
        $output = [
            'x' => $xMin,
            'y' => $yMin,
            'w' => 0,
            'h' => 0
        ];

        foreach ($detectedData as $data) {
            $width = ($data['x'] + $data['w']) - $output['x'];
            $height = ($data['y'] + $data['h']) - $output['y'];
            $output['w'] = max($output['w'], $width);
            $output['h'] = max($output['h'], $height);
        }

        // Debug mode - draw detected rectangles
        if ($this->config['debug']) {

            $canvas = imagecreatefromjpeg($imageFilePath);
            $colorRed = imagecolorallocate($canvas, 255, 0, 0);
            imagesetthickness($canvas, 3);

            foreach ($detectedData as $data) {
                imagerectangle(
                    $canvas,
                    $data['x'],
                    $data['y'],
                    $data['x'] + $data['w'],
                    $data['y'] + $data['h'],
                    $colorRed
                );
            }

            if (count($detectedData) > 1) {

                $colorWhite = imagecolorallocate($canvas, 255, 255, 255);

                imagerectangle(
                    $canvas,
                    $output['x'],
                    $output['y'],
                    $output['x'] + $output['w'],
                    $output['y'] + $output['h'],
                    $colorWhite
                );

            }

            header('Content-Type: image/jpeg');

            imagejpeg($canvas);
            imagedestroy($canvas);
            exit;
        }

        return $output;
    }

    /**
     * @param array $values
     * @return mixed
     */
    public function getValueRandom($values)
    {
        return $values[mt_rand(0, count($values) - 1)];
    }

    /**
     * @param array $values
     * @param int $currentIndex
     * @return mixed
     */
    public function getValueRounded($values, $currentIndex)
    {
        $index = $currentIndex % count($values);
        return $values[$index];
    }

}
