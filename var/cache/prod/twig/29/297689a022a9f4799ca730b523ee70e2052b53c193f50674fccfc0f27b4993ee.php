<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/homepage.html.twig */
class __TwigTemplate_ba4d06674447da69b08da7c5a871bbf4b44e1bc1121b32b6bd8356a28e901908 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin/homepage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Projects";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/tabler-ui/dist/assets/css/dashboard.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    <div class=\"container py-5\">

        <div class=\"row justify-content-md-center\">
            <div class=\"col col-lg-10\">

                <div class=\"card\">
                    <div class=\"card-body\">

                        <h1>";
        // line 19
        echo twig_escape_filter($this->env, ($context["app_name"] ?? null), "html", null, true);
        echo "</h1>
                        <hr>
                        <h3>";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Projects"), "html", null, true);
        echo "</h3>

                        ";
        // line 23
        $this->loadTemplate("default/flash_messages.html.twig", "admin/homepage.html.twig", 23)->display($context);
        // line 24
        echo "
                        ";
        // line 25
        if ((twig_length_filter($this->env, ($context["projects"] ?? null)) > 0)) {
            // line 26
            echo "
                            <table class=\"table table-bordered table-hover\">
                                <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Project"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Status"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Updated Time"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Actions"), "html", null, true);
            echo "</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["projects"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
                // line 39
                echo "                                        <tr>
                                            <td>";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "id", [], "any", false, false, false, 40), "html", null, true);
                echo "</td>
                                            <td>
                                                <a href=\"";
                // line 42
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
                echo "#/project/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "uniqueId", [], "any", false, false, false, 42), "html", null, true);
                echo "\" target=\"_blank\">
                                                    ";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "uniqueId", [], "any", false, false, false, 43), "html", null, true);
                echo "
                                                </a>
                                            </td>
                                            <td>";
                // line 46
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "status", [], "any", false, false, false, 46), "html", null, true);
                echo "</td>
                                            <td>
                                                ";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "updatedAt", [], "any", false, false, false, 48), "m/d/Y h:i:s"), "html", null, true);
                echo "
                                            </td>
                                            <td class=\"text-center\">
                                                <a href=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_delete_project", ["id" => twig_get_attribute($this->env, $this->source, $context["project"], "id", [], "any", false, false, false, 51)]), "html", null, true);
                echo "\" class=\"text-danger\" title=\"Delete\">
                                                    <i class=\"fe fe-x\"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                                </tbody>
                            </table>

                            ";
            // line 60
            $this->loadTemplate("nav/pagination_simple.html.twig", "admin/homepage.html.twig", 60)->display($context);
            // line 61
            echo "
                        ";
        } else {
            // line 63
            echo "                            <div class=\"alert alert-info my-7\">
                                ";
            // line 64
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Empty"), "html", null, true);
            echo ".
                            </div>
                        ";
        }
        // line 67
        echo "
                        <hr>
                        <a href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Home page"), "html", null, true);
        echo "</a>
                        &nbsp;&bull;&nbsp;
                        <a href=\"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_profile");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Profile"), "html", null, true);
        echo "</a>
                        &nbsp;&bull;&nbsp;
                        <a href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Log Out"), "html", null, true);
        echo "</a>

                    </div>
                </div>

            </div>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "admin/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 73,  202 => 71,  195 => 69,  191 => 67,  185 => 64,  182 => 63,  178 => 61,  176 => 60,  171 => 57,  159 => 51,  153 => 48,  148 => 46,  142 => 43,  136 => 42,  131 => 40,  128 => 39,  124 => 38,  117 => 34,  113 => 33,  109 => 32,  105 => 31,  98 => 26,  96 => 25,  93 => 24,  91 => 23,  86 => 21,  81 => 19,  70 => 10,  66 => 9,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/homepage.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\admin\\homepage.html.twig");
    }
}
