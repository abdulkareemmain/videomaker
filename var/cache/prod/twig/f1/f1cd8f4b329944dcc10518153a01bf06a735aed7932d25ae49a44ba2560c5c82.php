<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/index.html.twig */
class __TwigTemplate_7ae012550bc80213bfaee695e83d0cf5a4d605de6fef053f7accea851367fd39 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"app-container py-5\">
        <div class=\"container pt-md-5\">

            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"card card-body mt-5 bg-white-transp rounded\">

                        <h1 class=\"text-center\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Video Slide Show Maker"), "html", null, true);
        echo "</h1>

                        <app-root><div id=\"page-preloader\" class=\"loading\"></div></app-root>

                    </div>
                </div>
            </div>

        </div>
    </div>
";
    }

    // line 23
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "environment", [], "any", false, false, false, 24) == "prod")) {
            // line 25
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/styles.css"), "html", null, true);
            echo "\" rel=\"stylesheet\">
    ";
        }
    }

    // line 29
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    <script>
        var appSettings = {
            appName: '";
        // line 32
        echo twig_escape_filter($this->env, ($context["app_name"] ?? null), "html", null, true);
        echo "',
            webApiUrl: '";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 33), "baseUrl", [], "any", false, false, false, 33), "html", null, true);
        echo "',
            environment: '";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "environment", [], "any", false, false, false, 34), "html", null, true);
        echo "',
            locale: '";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 35), "getLocale", [], "method", false, false, false, 35), "html", null, true);
        echo "',
            displayLanguageSwitch: ";
        // line 36
        if (($context["display_language_switch"] ?? null)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
            displayHomepageVideo: ";
        // line 37
        if (($context["display_homepage_video"] ?? null)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
            displayHomepageFeatures: ";
        // line 38
        if (($context["display_homepage_features"] ?? null)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
            languages: ";
        // line 39
        echo json_encode(($context["languages"] ?? null));
        echo "
        };
    </script>
    <script type=\"text/javascript\" src=\"/assets/i18n/";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 42), "getLocale", [], "method", false, false, false, 42), "html", null, true);
        echo ".js?v=";
        echo twig_escape_filter($this->env, ($context["app_version"] ?? null), "html", null, true);
        echo "\"></script>
    ";
        // line 43
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "environment", [], "any", false, false, false, 43) == "prod")) {
            // line 44
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/runtime.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/polyfills.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/scripts.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/main.js"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // line 49
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/runtime.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/polyfills.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/styles.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/scripts.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/vendor.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/main.js"), "html", null, true);
            echo "\"></script>
    ";
        }
    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 54,  188 => 53,  184 => 52,  180 => 51,  176 => 50,  171 => 49,  166 => 47,  162 => 46,  158 => 45,  153 => 44,  151 => 43,  145 => 42,  139 => 39,  131 => 38,  123 => 37,  115 => 36,  111 => 35,  107 => 34,  103 => 33,  99 => 32,  95 => 30,  91 => 29,  83 => 25,  80 => 24,  76 => 23,  61 => 11,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/index.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\default\\index.html.twig");
    }
}
