<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/index.html.twig */
class __TwigTemplate_09944dc339179cc34de38b3978254cf5b2af985458547cb3616fd6a46fe56e1f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"app-container py-5\">
        <div class=\"container pt-md-5\">

            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"card card-body mt-5 bg-white-transp rounded\">

                        <h1 class=\"text-center\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Video Slide Show Maker"), "html", null, true);
        echo "</h1>

                        <app-root><div id=\"page-preloader\" class=\"loading\"></div></app-root>

                    </div>
                </div>
            </div>

        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 23
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 24
        echo "    ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 24, $this->source); })()), "environment", [], "any", false, false, false, 24) == "prod")) {
            // line 25
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/styles.css"), "html", null, true);
            echo "\" rel=\"stylesheet\">
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 30
        echo "    <script>
        var appSettings = {
            appName: '";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["app_name"]) || array_key_exists("app_name", $context) ? $context["app_name"] : (function () { throw new RuntimeError('Variable "app_name" does not exist.', 32, $this->source); })()), "html", null, true);
        echo "',
            webApiUrl: '";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 33, $this->source); })()), "request", [], "any", false, false, false, 33), "baseUrl", [], "any", false, false, false, 33), "html", null, true);
        echo "',
            environment: '";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 34, $this->source); })()), "environment", [], "any", false, false, false, 34), "html", null, true);
        echo "',
            locale: '";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 35, $this->source); })()), "request", [], "any", false, false, false, 35), "getLocale", [], "method", false, false, false, 35), "html", null, true);
        echo "',
            displayLanguageSwitch: ";
        // line 36
        if ((isset($context["display_language_switch"]) || array_key_exists("display_language_switch", $context) ? $context["display_language_switch"] : (function () { throw new RuntimeError('Variable "display_language_switch" does not exist.', 36, $this->source); })())) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
            displayHomepageVideo: ";
        // line 37
        if ((isset($context["display_homepage_video"]) || array_key_exists("display_homepage_video", $context) ? $context["display_homepage_video"] : (function () { throw new RuntimeError('Variable "display_homepage_video" does not exist.', 37, $this->source); })())) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
            displayHomepageFeatures: ";
        // line 38
        if ((isset($context["display_homepage_features"]) || array_key_exists("display_homepage_features", $context) ? $context["display_homepage_features"] : (function () { throw new RuntimeError('Variable "display_homepage_features" does not exist.', 38, $this->source); })())) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
            languages: ";
        // line 39
        echo json_encode((isset($context["languages"]) || array_key_exists("languages", $context) ? $context["languages"] : (function () { throw new RuntimeError('Variable "languages" does not exist.', 39, $this->source); })()));
        echo "
        };
    </script>
    <script type=\"text/javascript\" src=\"/assets/i18n/";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "request", [], "any", false, false, false, 42), "getLocale", [], "method", false, false, false, 42), "html", null, true);
        echo ".js?v=";
        echo twig_escape_filter($this->env, (isset($context["app_version"]) || array_key_exists("app_version", $context) ? $context["app_version"] : (function () { throw new RuntimeError('Variable "app_version" does not exist.', 42, $this->source); })()), "html", null, true);
        echo "\"></script>
    ";
        // line 43
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "environment", [], "any", false, false, false, 43) == "prod")) {
            // line 44
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/runtime.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/polyfills.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/scripts.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/main.js"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // line 49
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/runtime.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/polyfills.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/styles.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/scripts.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/vendor.js"), "html", null, true);
            echo "\"></script>
        <script type=\"text/javascript\" src=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle-dev/main.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 54,  230 => 53,  226 => 52,  222 => 51,  218 => 50,  213 => 49,  208 => 47,  204 => 46,  200 => 45,  195 => 44,  193 => 43,  187 => 42,  181 => 39,  173 => 38,  165 => 37,  157 => 36,  153 => 35,  149 => 34,  145 => 33,  141 => 32,  137 => 30,  127 => 29,  113 => 25,  110 => 24,  100 => 23,  79 => 11,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"app-container py-5\">
        <div class=\"container pt-md-5\">

            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"card card-body mt-5 bg-white-transp rounded\">

                        <h1 class=\"text-center\">{{ 'Video Slide Show Maker' | trans }}</h1>

                        <app-root><div id=\"page-preloader\" class=\"loading\"></div></app-root>

                    </div>
                </div>
            </div>

        </div>
    </div>
{% endblock %}

{% block stylesheets %}
    {% if app.environment == 'prod' %}
        <link href=\"{{ asset('bundle/styles.css') }}\" rel=\"stylesheet\">
    {% endif %}
{% endblock %}

{% block javascripts %}
    <script>
        var appSettings = {
            appName: '{{ app_name }}',
            webApiUrl: '{{ app.request.baseUrl }}',
            environment: '{{ app.environment }}',
            locale: '{{ app.request.getLocale() }}',
            displayLanguageSwitch: {% if display_language_switch %}true{% else %}false{% endif %},
            displayHomepageVideo: {% if display_homepage_video %}true{% else %}false{% endif %},
            displayHomepageFeatures: {% if display_homepage_features %}true{% else %}false{% endif %},
            languages: {{ languages | json_encode | raw }}
        };
    </script>
    <script type=\"text/javascript\" src=\"/assets/i18n/{{ app.request.getLocale() }}.js?v={{ app_version }}\"></script>
    {% if app.environment == 'prod' %}
        <script type=\"text/javascript\" src=\"{{ asset('bundle/runtime.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle/polyfills.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle/scripts.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle/main.js') }}\"></script>
    {% else %}
        <script type=\"text/javascript\" src=\"{{ asset('bundle-dev/runtime.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle-dev/polyfills.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle-dev/styles.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle-dev/scripts.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle-dev/vendor.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundle-dev/main.js') }}\"></script>
    {% endif %}
{% endblock %}", "default/index.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\default\\index.html.twig");
    }
}
