<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'homepage', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/api/media' => [[['_route' => 'media_upload', '_controller' => 'App\\Controller\\DefaultController::mediaUploadAction'], null, null, null, false, false, null]],
        '/api/library_music' => [[['_route' => 'library_music', '_controller' => 'App\\Controller\\DefaultController::libraryMusicAction'], null, ['GET' => 0], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/profile' => [[['_route' => 'app_profile', '_controller' => 'App\\Controller\\SecurityController::updateProfileAction'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/a(?'
                    .'|dmin(?'
                        .'|(?:/(\\d+))?(*:30)'
                        .'|/delete/(\\d+)(*:50)'
                    .')'
                    .'|pi/project(?'
                        .'|/([^/]++)(?'
                            .'|(*:83)'
                            .'|/upload(*:97)'
                        .')'
                        .'|_progress/([^/]++)(*:123)'
                    .')'
                .')'
                .'|/switch_locale(?:/([^/]++))?(*:161)'
                .'|/project_download/([^/]++)(*:195)'
                .'|/media/cache/resolve/(?'
                    .'|([A-z0-9_-]*)/rc/([^/]++)/(.+)(*:257)'
                    .'|([A-z0-9_-]*)/(.+)(*:283)'
                .')'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:323)'
                    .'|wdt/([^/]++)(*:343)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:389)'
                            .'|router(*:403)'
                            .'|exception(?'
                                .'|(*:423)'
                                .'|\\.css(*:436)'
                            .')'
                        .')'
                        .'|(*:446)'
                    .')'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        30 => [[['_route' => 'admin_homepage', 'page' => '1', '_controller' => 'App\\Controller\\Admin\\DefaultController::homeAction'], ['page'], null, null, false, true, null]],
        50 => [[['_route' => 'admin_delete_project', '_controller' => 'App\\Controller\\Admin\\DefaultController::deleteProjectAction'], ['id'], null, null, false, true, null]],
        83 => [
            [['_route' => 'project_get', '_controller' => 'App\\Controller\\ProjectController::indexAction'], ['uniqueId'], ['GET' => 0], null, false, true, null],
            [['_route' => 'project_update', '_controller' => 'App\\Controller\\ProjectController::updateAction'], ['uniqueId'], ['POST' => 0], null, false, true, null],
        ],
        97 => [[['_route' => 'project_upload_file', '_controller' => 'App\\Controller\\ProjectController::uploadFileAction'], ['uniqueId'], ['POST' => 0], null, false, false, null]],
        123 => [[['_route' => 'project_progress', '_controller' => 'App\\Controller\\ProjectController::progressAction'], ['uniqueId'], ['GET' => 0], null, false, true, null]],
        161 => [[['_route' => 'switch_locale', 'locale' => '', '_controller' => 'App\\Controller\\DefaultController::switchLocaleAction'], ['locale'], ['POST' => 0], null, false, true, null]],
        195 => [[['_route' => 'project_download', '_controller' => 'App\\Controller\\ProjectController::downloadAction'], ['uniqueId'], ['GET' => 0], null, false, true, null]],
        257 => [[['_route' => 'liip_imagine_filter_runtime', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterRuntimeAction'], ['filter', 'hash', 'path'], ['GET' => 0], null, false, true, null]],
        283 => [[['_route' => 'liip_imagine_filter', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterAction'], ['filter', 'path'], ['GET' => 0], null, false, true, null]],
        323 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        343 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        389 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        403 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        423 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        436 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        446 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
