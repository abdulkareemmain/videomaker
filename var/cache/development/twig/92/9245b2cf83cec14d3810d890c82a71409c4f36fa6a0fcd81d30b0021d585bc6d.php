<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/homepage.html.twig */
class __TwigTemplate_e7bdef7db3993f608985655dc9de7feb2f31cd44078f956e9c111a4f99bc265d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/homepage.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/homepage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Projects";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/tabler-ui/dist/assets/css/dashboard.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "
    <div class=\"container py-5\">

        <div class=\"row justify-content-md-center\">
            <div class=\"col col-lg-10\">

                <div class=\"card\">
                    <div class=\"card-body\">

                        <h1>";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["app_name"]) || array_key_exists("app_name", $context) ? $context["app_name"] : (function () { throw new RuntimeError('Variable "app_name" does not exist.', 19, $this->source); })()), "html", null, true);
        echo "</h1>
                        <hr>
                        <h3>";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Projects"), "html", null, true);
        echo "</h3>

                        ";
        // line 23
        $this->loadTemplate("default/flash_messages.html.twig", "admin/homepage.html.twig", 23)->display($context);
        // line 24
        echo "
                        ";
        // line 25
        if ((twig_length_filter($this->env, (isset($context["projects"]) || array_key_exists("projects", $context) ? $context["projects"] : (function () { throw new RuntimeError('Variable "projects" does not exist.', 25, $this->source); })())) > 0)) {
            // line 26
            echo "
                            <table class=\"table table-bordered table-hover\">
                                <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Project"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Status"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Updated Time"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Actions"), "html", null, true);
            echo "</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["projects"]) || array_key_exists("projects", $context) ? $context["projects"] : (function () { throw new RuntimeError('Variable "projects" does not exist.', 38, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
                // line 39
                echo "                                        <tr>
                                            <td>";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "id", [], "any", false, false, false, 40), "html", null, true);
                echo "</td>
                                            <td>
                                                <a href=\"";
                // line 42
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
                echo "#/project/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "uniqueId", [], "any", false, false, false, 42), "html", null, true);
                echo "\" target=\"_blank\">
                                                    ";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "uniqueId", [], "any", false, false, false, 43), "html", null, true);
                echo "
                                                </a>
                                            </td>
                                            <td>";
                // line 46
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "status", [], "any", false, false, false, 46), "html", null, true);
                echo "</td>
                                            <td>
                                                ";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "updatedAt", [], "any", false, false, false, 48), "m/d/Y h:i:s"), "html", null, true);
                echo "
                                            </td>
                                            <td class=\"text-center\">
                                                <a href=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_delete_project", ["id" => twig_get_attribute($this->env, $this->source, $context["project"], "id", [], "any", false, false, false, 51)]), "html", null, true);
                echo "\" class=\"text-danger\" title=\"Delete\">
                                                    <i class=\"fe fe-x\"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                                </tbody>
                            </table>

                            ";
            // line 60
            $this->loadTemplate("nav/pagination_simple.html.twig", "admin/homepage.html.twig", 60)->display($context);
            // line 61
            echo "
                        ";
        } else {
            // line 63
            echo "                            <div class=\"alert alert-info my-7\">
                                ";
            // line 64
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Empty"), "html", null, true);
            echo ".
                            </div>
                        ";
        }
        // line 67
        echo "
                        <hr>
                        <a href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Home page"), "html", null, true);
        echo "</a>
                        &nbsp;&bull;&nbsp;
                        <a href=\"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_profile");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Profile"), "html", null, true);
        echo "</a>
                        &nbsp;&bull;&nbsp;
                        <a href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Log Out"), "html", null, true);
        echo "</a>

                    </div>
                </div>

            </div>
        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "admin/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 73,  223 => 71,  216 => 69,  212 => 67,  206 => 64,  203 => 63,  199 => 61,  197 => 60,  192 => 57,  180 => 51,  174 => 48,  169 => 46,  163 => 43,  157 => 42,  152 => 40,  149 => 39,  145 => 38,  138 => 34,  134 => 33,  130 => 32,  126 => 31,  119 => 26,  117 => 25,  114 => 24,  112 => 23,  107 => 21,  102 => 19,  91 => 10,  84 => 9,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Projects{% endblock %}

{% block stylesheets %}
    <link href=\"{{ asset('assets/css/tabler-ui/dist/assets/css/dashboard.css') }}\" rel=\"stylesheet\">
{% endblock %}

{% block body %}

    <div class=\"container py-5\">

        <div class=\"row justify-content-md-center\">
            <div class=\"col col-lg-10\">

                <div class=\"card\">
                    <div class=\"card-body\">

                        <h1>{{ app_name }}</h1>
                        <hr>
                        <h3>{{ 'Projects' | trans }}</h3>

                        {% include 'default/flash_messages.html.twig' %}

                        {% if projects | length > 0 %}

                            <table class=\"table table-bordered table-hover\">
                                <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>{{ 'Project' | trans }}</td>
                                        <td>{{ 'Status' | trans }}</td>
                                        <td>{{ 'Updated Time' | trans }}</td>
                                        <td>{{ 'Actions' | trans }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {% for project in projects %}
                                        <tr>
                                            <td>{{ project.id }}</td>
                                            <td>
                                                <a href=\"{{ path('homepage') }}#/project/{{ project.uniqueId }}\" target=\"_blank\">
                                                    {{ project.uniqueId }}
                                                </a>
                                            </td>
                                            <td>{{ project.status }}</td>
                                            <td>
                                                {{ project.updatedAt | date(\"m/d/Y h:i:s\") }}
                                            </td>
                                            <td class=\"text-center\">
                                                <a href=\"{{ path('admin_delete_project', {id: project.id}) }}\" class=\"text-danger\" title=\"Delete\">
                                                    <i class=\"fe fe-x\"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    {% endfor %}
                                </tbody>
                            </table>

                            {% include 'nav/pagination_simple.html.twig' %}

                        {% else %}
                            <div class=\"alert alert-info my-7\">
                                {{ 'Empty' | trans }}.
                            </div>
                        {% endif %}

                        <hr>
                        <a href=\"{{ path('homepage') }}\">{{ 'Home page' | trans }}</a>
                        &nbsp;&bull;&nbsp;
                        <a href=\"{{ path('app_profile') }}\">{{ 'Profile' | trans }}</a>
                        &nbsp;&bull;&nbsp;
                        <a href=\"{{ path('app_logout') }}\">{{ 'Log Out' | trans }}</a>

                    </div>
                </div>

            </div>
        </div>

    </div>

{% endblock %}
", "admin/homepage.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\admin\\homepage.html.twig");
    }
}
