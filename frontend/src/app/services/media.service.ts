import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataService} from './data-service.abstract';
import {Media} from '../models/media.model';

@Injectable({
    providedIn: 'root'
})
export class MediaService extends DataService<Media> {

    constructor(http: HttpClient) {
        super(http);
        this.setRequestUrl('api/media1');
    }

}
