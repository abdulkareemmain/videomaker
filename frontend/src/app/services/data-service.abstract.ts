import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {QueryOptions} from '../models/query-options.model';
import {DataList} from '../models/data-list.interface';
import {SimpleEntity} from '../models/simple-entity.interface';

export interface OutputData {
    data: any | any[] | null;
    successMsg: string;
    errorMsg: string;
    total: number;
}

export abstract class DataService<M extends SimpleEntity> {

    public headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
    });
    private requestUrl = '';

    constructor(
        public http: HttpClient
    ) {
        this.requestUrl = 'app/data_list';
    }

    setRequestUrl(url): void {
        this.requestUrl = url;
    }

    getRequestUrl(): string {
        return this.requestUrl;
    }

    getRequestFullUrl(action?: string, id?: string|number): string {
        switch (action) {
            case 'get_one':
            case 'delete':
                return this.getRequestUrl() + `/${id}`;
            case 'update':
                return this.getRequestUrl() + `/${id}/update`;
            case 'upload':
                return this.getRequestUrl() + `/${id}/upload`;
            default:
                return this.getRequestUrl();
        }
    }

    getParams(options: any): HttpParams {
        let params = new HttpParams();
        for (const name in options) {
            if (!options.hasOwnProperty(name)
                || typeof options[name] === 'undefined') {
                continue;
            }
            params = params.append(name, options[name]);
        }
        return params;
    }

    getItem(id: number|string): Observable<M> {
        const url = this.getRequestUrl() + `/${id}`;
        return this.http.get<M>(url, {headers: this.headers}).pipe(
            catchError(this.handleError<M>())
        );
    }

    getList(options ?: QueryOptions): Observable<M[]> {
        let params = new HttpParams();
        for (const name in options) {
            if (!options.hasOwnProperty(name)
                || typeof options[name] === 'undefined') {
                continue;
            }
            params = params.append(name, options[name]);
        }
        return this.http.get<M[]>(this.getRequestUrl(), {params: params, headers: this.headers})
            .pipe(
                catchError(this.handleError<any>())
            );
    }

    getListPage(options ?: QueryOptions): Observable<DataList<M>> {
        let params = new HttpParams();
        for (const name in options) {
            if (!options.hasOwnProperty(name)
                || typeof options[name] === 'undefined') {
                continue;
            }
            params = params.append(name, options[name]);
        }
        return this.http.get<DataList<M>>(this.getRequestUrl(), {params: params, headers: this.headers})
            .pipe(
                catchError(this.handleError<any>())
            );
    }

    deleteItem(id: number): Observable<M> {
        const url = this.getRequestUrl() + `/${id}`;
        return this.http.delete<M>(url, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    deleteByArray(idsArray: number[]): Observable<M> {
        const url = this.getRequestUrl() + '/batch';
        const data = {ids: idsArray};
        return this.http.post<M>(url, data, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    create(item: M): Observable<M> {
        return this.http.post<M>(this.getRequestUrl(), item, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    update(item: M): Observable<M> {
        const url = this.getRequestUrl() + `/${item.id}`;
        return this.http.put<M>(url, item, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    updateProperty(itemId: number, optionsName: string, value: string | number): Observable<M> {
        const url = this.getRequestUrl() + `/update/${optionsName}/${itemId}`;
        return this.http.patch<M>(url, {value: value}, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    getFormData(item: any): FormData {
        const formData: FormData = new FormData();
        Object.keys(item).forEach((key) => {
            if (item[key] instanceof File) {
                formData.append(key, item[key], item[key].name);
            } else if (typeof item[key] !== 'undefined') {
                if (typeof item[key] === 'boolean') {
                    formData.append(key, item[key] ? '1' : '0');
                } else {
                    formData.append(key, item[key] || '');
                }
            }
        });
        return formData;
    }

    postFormData(formData: FormData, url?: string): Observable<M> {
        url = url || this.getRequestFullUrl();
        const headers = new HttpHeaders({
            'enctype': 'multipart/form-data',
            'Accept': 'application/json'
        });
        return this.http
            .post<M>(url, formData, {headers: headers})
            .pipe(
                catchError(this.handleError<M>())
            );
    }

    handleError<T> (operation = 'operation', result?: T) {
        return (err: any): Observable<T> => {
            if (err.error) {
                throw err.error;
            }
            return of(result as T);
        };
    }
}