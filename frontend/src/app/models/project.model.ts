export class Project {
    constructor(
        public uniqueId: string,
        public status: string,
        public id?: number,
        public options?: {[key: string]: string|number}
    ) {
        if (typeof this.options === 'undefined') {
            this.options = {};
        }
    }
}
