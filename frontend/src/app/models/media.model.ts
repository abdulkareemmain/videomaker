export class Media {

    static getMediaFullUrl(fileName: string, projectUniqueId: string): string {
        return `userfiles/projects/${projectUniqueId}/${fileName}`;
    }

    static getMediaResizedUrl(media: Media, projectUniqueId: string, filterSet = 'squared_thumbnail'): string {
        return Media.getResizedUrl(Media.getMediaFullUrl(media.fileName, projectUniqueId), filterSet);
    }

    static getResizedUrl(imageUrl: string, filterSet = 'squared_thumbnail'): string {
        const resizeResolveUrl = `media/cache/resolve/${filterSet}/`;
        return resizeResolveUrl + imageUrl;
    }

    static getExtension(url) {
        if (url.indexOf('.') === -1) {
            return '';
        }
        url = Media.baseName(url);
        const tmp = url.toLowerCase().split('.');
        return (tmp[tmp.length - 1]).toLowerCase();
    }

    static baseName(url) {
        if (url.indexOf('/') > -1) {
            return url.substr(url.lastIndexOf('/') + 1);
        }
        return url;
    }

    constructor(
        public id: number,
        public title: string,
        public fileName: string,
        public isActive?: boolean,
        public orderIndex?: number,
        public textTitle?: string,
        public options?: {[key: string]: string|number|boolean},
        public motionOptions?: {from: {[key: string]: string|number}, to: {[key: string]: string|number}}
    ) {
        if (typeof this.options === 'undefined') {
            this.options = {};
        }
        if (typeof this.motionOptions === 'undefined') {
            this.motionOptions = {from: {}, to: {}};
        }
    }
}
