(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./homepage/homepage.component */ "./src/app/homepage/homepage.component.ts");
/* harmony import */ var _project_project_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./project/project.component */ "./src/app/project/project.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_3__["HomepageComponent"]
    },
    {
        path: 'project/:uniqueId',
        component: _project_project_component__WEBPACK_IMPORTED_MODULE_4__["ProjectComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true })
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"position-fixed\" style=\"top: 15px; right: 25px;\" *ngIf=\"displayLanguageSwitch\">\n    <ng-container *ngFor=\"let language of languages\">\n        <div class=\"d-inline-block ml-3\" *ngIf=\"locale != language.code\">\n            <a class=\"text-white\" href=\"#\" (click)=\"switchLocale(language.code, $event)\">\n                <i [class]=\"'flag ' + language.icon\"></i>\n                {{ language.name }}\n            </a>\n        </div>\n    </ng-container>\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(httpClient, translate) {
        this.httpClient = httpClient;
        this.translate = translate;
        this.locale = 'en';
        this.languages = [];
        this.locale = appSettings.locale;
        this.displayLanguageSwitch = appSettings.displayLanguageSwitch;
        this.displayHomepageVideo = appSettings.displayHomepageVideo;
        this.displayHomepageFeatures = appSettings.displayHomepageFeatures;
        this.languages = appSettings.languages;
        this.translate.addLangs(['en', 'ru']);
        this.translate.setDefaultLang('en');
        this.translate.use(this.locale);
    }
    AppComponent.prototype.ngOnInit = function () {
        var brusher = new Brusher({
            image: 'assets/img/background6.jpg',
            keepCleared: false,
            stroke: 80,
            lineStyle: 'round'
        });
        brusher.init();
    };
    AppComponent.prototype.switchLocale = function (locale, event) {
        var _this = this;
        if (event) {
            event.preventDefault();
        }
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        });
        this.httpClient.post("/switch_locale/" + locale, {}, { headers: headers })
            .subscribe(function (res) {
            if (res['success']) {
                _this.pageReload();
            }
        });
    };
    AppComponent.prototype.pageReload = function () {
        window.location.reload();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_dragula__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-dragula */ "./node_modules/ng2-dragula/dist/fesm5/ng2-dragula.js");
/* harmony import */ var angular_cropperjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-cropperjs */ "./node_modules/angular-cropperjs/fesm5/angular-cropperjs.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _project_project_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./project/project.component */ "./src/app/project/project.component.ts");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./homepage/homepage.component */ "./src/app/homepage/homepage.component.ts");
/* harmony import */ var _project_modal_edit_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./project/modal-edit.component */ "./src/app/project/modal-edit.component.ts");
/* harmony import */ var _angular_common_locales_en__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common/locales/en */ "./node_modules/@angular/common/locales/en.js");
/* harmony import */ var _angular_common_locales_en__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_en__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _angular_common_locales_ru__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/locales/ru */ "./node_modules/@angular/common/locales/ru.js");
/* harmony import */ var _angular_common_locales_ru__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_ru__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _services_translateLoader__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/translateLoader */ "./src/app/services/translateLoader.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















Object(_angular_common__WEBPACK_IMPORTED_MODULE_4__["registerLocaleData"])(_angular_common_locales_en__WEBPACK_IMPORTED_MODULE_14___default.a, 'en-EN');
Object(_angular_common__WEBPACK_IMPORTED_MODULE_4__["registerLocaleData"])(_angular_common_locales_ru__WEBPACK_IMPORTED_MODULE_15___default.a, 'ru-RU');
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"],
                _project_project_component__WEBPACK_IMPORTED_MODULE_11__["ProjectComponent"],
                _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_12__["HomepageComponent"],
                _project_modal_edit_component__WEBPACK_IMPORTED_MODULE_13__["ModalPhotoEditComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateLoader"],
                        useClass: _services_translateLoader__WEBPACK_IMPORTED_MODULE_16__["TranslateCustomLoader"]
                    }
                }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"],
                angular_cropperjs__WEBPACK_IMPORTED_MODULE_7__["AngularCropperjsModule"],
                ng2_dragula__WEBPACK_IMPORTED_MODULE_6__["DragulaModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["ModalModule"].forRoot()
            ],
            entryComponents: [
                _project_modal_edit_component__WEBPACK_IMPORTED_MODULE_13__["ModalPhotoEditComponent"]
            ],
            exports: [
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/homepage/homepage.component.css":
/*!*************************************************!*\
  !*** ./src/app/homepage/homepage.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVwYWdlL2hvbWVwYWdlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/homepage/homepage.component.html":
/*!**************************************************!*\
  !*** ./src/app/homepage/homepage.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"dimmer\" [ngClass]=\"{'active': loading}\">\n    <div class=\"loader\"></div>\n    <div class=\"dimmer-content\">\n        <div class=\"my-5 text-center\">\n            <input type=\"file\" class=\"d-none\" accept=\"image/*\" multiple #fileInput (change)=\"onFileSelect()\">\n            <button type=\"button\" class=\"btn btn-pill btn-success btn-lg\"\n                    (click)=\"selectFiles($event)\"\n                    (drop)=\"dropHandler($event)\"\n                    (dragover)=\"dragOverHandler($event);\">\n                {{'SELECT_PHOTOS' | translate}}\n            </button>\n\n            <ng-container *ngIf=\"files.length > 0\">\n                <div class=\"pt-5\">\n                    {{'SELECTED_FILES' | translate}}: {{files.length}}\n                </div>\n                <div class=\"pt-5\">\n                    <button type=\"button\" class=\"btn btn-pill btn-primary btn-lg\" (click)=\"uploadFiles()\">\n                        {{'UPLOAD_PHOTOS' | translate}}\n                    </button>\n                    <button type=\"button\" class=\"btn btn-pill btn-secondary btn-lg ml-3\" (click)=\"clearFiles()\">\n                        {{'CANCEL' | translate}}\n                    </button>\n                </div>\n            </ng-container>\n\n            <div class=\"pt-5\" [hidden]=\"!isSampleVideoVisible\" *ngIf=\"displayHomepageVideo\">\n                <div class=\"bg-dark\">\n                    <video class=\"d-block\" src=\"/assets/video/slideshow-sample.mp4\" controls style=\"width: 100%; height: 408px;\" #sampleVideo></video>\n                </div>\n                <div class=\"position-relative\">\n                    <div class=\"position-absolute\" style=\"top: -390px; right: 15px;\">\n                        <button type=\"button\" class=\"btn btn-pill btn-secondary btn-lg\" (click)=\"sampleVideoHide($event)\">\n                            {{'HIDE' | translate}}\n                        </button>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n\n        <div class=\"row mt-5 pt-5 text-black-50\" *ngIf=\"displayHomepageFeatures\">\n            <div class=\"col-md-4 mb-3\">\n                <div class=\"text-center\">\n                    <div class=\"mb-2\">\n                        <i class=\"fe fe-watch font-big\"></i>\n                    </div>\n                    <h4>{{'FAST' | translate}}</h4>\n                    <div>\n                        {{'FAST_DESC' | translate}}\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4 mb-3\">\n                <div class=\"text-center\">\n                    <div class=\"mb-2\">\n                        <i class=\"fe fe-shield font-big\"></i>\n                    </div>\n                    <h4>{{'SAFELY' | translate}}</h4>\n                    <div>\n                        {{'SAFELY_DESC' | translate}}\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4 mb-3\">\n                <div class=\"text-center\">\n                    <div class=\"mb-2\">\n                        <i class=\"fe fe-thumbs-up font-big\"></i>\n                    </div>\n                    <h4>{{'FREE' | translate}}</h4>\n                    <div>\n                        {{'FREE_DESC' | translate}}\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/homepage/homepage.component.ts":
/*!************************************************!*\
  !*** ./src/app/homepage/homepage.component.ts ***!
  \************************************************/
/*! exports provided: HomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponent", function() { return HomepageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_media_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/media.service */ "./src/app/services/media.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomepageComponent = /** @class */ (function () {
    function HomepageComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
        this.loading = false;
        this.files = [];
        this.isSampleVideoVisible = true;
        this.displayLanguageSwitch = appSettings.displayLanguageSwitch;
        this.displayHomepageVideo = appSettings.displayHomepageVideo;
        this.displayHomepageFeatures = appSettings.displayHomepageFeatures;
    }
    HomepageComponent.prototype.ngOnInit = function () {
    };
    HomepageComponent.prototype.selectFiles = function (event) {
        if (event) {
            event.preventDefault();
        }
        this.fileInput.nativeElement.click();
    };
    HomepageComponent.prototype.onFileSelect = function () {
        var _a;
        var files = this.fileInput.nativeElement.files;
        if (!files || files.length === 0) {
            return;
        }
        (_a = this.files).push.apply(_a, files);
    };
    HomepageComponent.prototype.dragOverHandler = function (event) {
        event.preventDefault();
    };
    HomepageComponent.prototype.dropHandler = function (event) {
        event.preventDefault();
        if (event.dataTransfer.items) {
            for (var i = 0; i < event.dataTransfer.items.length; i++) {
                this.files.push(event.dataTransfer.items[i].getAsFile());
            }
        }
        else {
            for (var i = 0; i < event.dataTransfer.files.length; i++) {
                this.files.push(event.dataTransfer.files[i]);
            }
        }
    };
    HomepageComponent.prototype.uploadFiles = function () {
        var _this = this;
        this.loading = true;
        var data = {};
        this.files.forEach(function (file, index) {
            data["file" + index] = file;
        });
        this.dataService.postFormData(this.dataService.getFormData(data))
            .subscribe(function (res) {
            if (res['success']) {
                _this.clearFiles();
                _this.router.navigate(['/project', res['projectUniqueId']]);
            }
            _this.loading = false;
        }, function (err) {
            _this.loading = false;
        });
    };
    HomepageComponent.prototype.clearFiles = function () {
        this.files.splice(0, this.files.length);
    };
    HomepageComponent.prototype.sampleVideoHide = function (event) {
        if (event) {
            event.preventDefault();
        }
        if (this.sampleVideo.nativeElement.tagName.toLowerCase() === 'video') {
            this.sampleVideo.nativeElement.pause();
        }
        else if (this.sampleVideo.nativeElement.tagName.toLowerCase() === 'div') {
            var iframe = this.sampleVideo.nativeElement.querySelector('iframe');
            iframe.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
        }
        this.isSampleVideoVisible = false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", Object)
    ], HomepageComponent.prototype, "fileInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sampleVideo'),
        __metadata("design:type", Object)
    ], HomepageComponent.prototype, "sampleVideo", void 0);
    HomepageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homepage',
            template: __webpack_require__(/*! ./homepage.component.html */ "./src/app/homepage/homepage.component.html"),
            styles: [__webpack_require__(/*! ./homepage.component.css */ "./src/app/homepage/homepage.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_media_service__WEBPACK_IMPORTED_MODULE_2__["MediaService"]])
    ], HomepageComponent);
    return HomepageComponent;
}());



/***/ }),

/***/ "./src/app/models/media.model.ts":
/*!***************************************!*\
  !*** ./src/app/models/media.model.ts ***!
  \***************************************/
/*! exports provided: Media */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Media", function() { return Media; });
var Media = /** @class */ (function () {
    function Media(id, title, fileName, isActive, orderIndex, textTitle, options, motionOptions) {
        this.id = id;
        this.title = title;
        this.fileName = fileName;
        this.isActive = isActive;
        this.orderIndex = orderIndex;
        this.textTitle = textTitle;
        this.options = options;
        this.motionOptions = motionOptions;
        if (typeof this.options === 'undefined') {
            this.options = {};
        }
        if (typeof this.motionOptions === 'undefined') {
            this.motionOptions = { from: {}, to: {} };
        }
    }
    Media.getMediaFullUrl = function (fileName, projectUniqueId) {
        return "userfiles/projects/" + projectUniqueId + "/" + fileName;
    };
    Media.getMediaResizedUrl = function (media, projectUniqueId, filterSet) {
        if (filterSet === void 0) { filterSet = 'squared_thumbnail'; }
        return Media.getResizedUrl(Media.getMediaFullUrl(media.fileName, projectUniqueId), filterSet);
    };
    Media.getResizedUrl = function (imageUrl, filterSet) {
        if (filterSet === void 0) { filterSet = 'squared_thumbnail'; }
        var resizeResolveUrl = "media/cache/resolve/" + filterSet + "/";
        return resizeResolveUrl + imageUrl;
    };
    Media.getExtension = function (url) {
        if (url.indexOf('.') === -1) {
            return '';
        }
        url = Media.baseName(url);
        var tmp = url.toLowerCase().split('.');
        return (tmp[tmp.length - 1]).toLowerCase();
    };
    Media.baseName = function (url) {
        if (url.indexOf('/') > -1) {
            return url.substr(url.lastIndexOf('/') + 1);
        }
        return url;
    };
    return Media;
}());



/***/ }),

/***/ "./src/app/models/project.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/project.model.ts ***!
  \*****************************************/
/*! exports provided: Project */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Project", function() { return Project; });
var Project = /** @class */ (function () {
    function Project(uniqueId, status, id, options) {
        this.uniqueId = uniqueId;
        this.status = status;
        this.id = id;
        this.options = options;
        if (typeof this.options === 'undefined') {
            this.options = {};
        }
    }
    return Project;
}());



/***/ }),

/***/ "./src/app/project/modal-edit.component.ts":
/*!*************************************************!*\
  !*** ./src/app/project/modal-edit.component.ts ***!
  \*************************************************/
/*! exports provided: ModalPhotoEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPhotoEditComponent", function() { return ModalPhotoEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var angular_cropperjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-cropperjs */ "./node_modules/angular-cropperjs/fesm5/angular-cropperjs.js");
/* harmony import */ var _models_media_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/media.model */ "./src/app/models/media.model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ModalPhotoEditComponent = /** @class */ (function () {
    function ModalPhotoEditComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
        this.position = 'from';
        this.isEditMode = false;
        this.isExtendMode = false;
    }
    ModalPhotoEditComponent.prototype.ngOnInit = function () {
        this.motionOptions = Object.assign({}, { from: {}, to: {} }, this.media.motionOptions);
        this.options = Object.assign({}, this.media.options);
        if (!this.options.duration) {
            this.options.duration = 300;
        }
        if (!this.options.text_position) {
            this.options.text_position = 'top_left';
        }
        if (!this.options.transition) {
            this.options.transition = 'fade';
        }
        this.isEditMode = this.media.options ? !!this.media.options['customized'] : false;
        this.cropperOptions = {
            aspectRatio: 1280 / 720,
            checkCrossOrigin: false,
            checkOrientation: true,
            autoCrop: false,
            data: this.motionOptions.from
        };
        this.imageUrl = _models_media_model__WEBPACK_IMPORTED_MODULE_3__["Media"].getMediaFullUrl(this.media.fileName, this.project.uniqueId);
    };
    ModalPhotoEditComponent.prototype.onCropperReady = function (event) {
        if (event && event.target) {
            this.cropperToggle();
        }
    };
    ModalPhotoEditComponent.prototype.onModeChange = function (value) {
        this.isEditMode = value;
        this.position = 'from';
        this.cropperToggle();
    };
    ModalPhotoEditComponent.prototype.cropperToggle = function () {
        if (this.isEditMode && this.angularCropper.cropper) {
            this.angularCropper.cropper.enable();
            this.angularCropper.cropper.crop();
            if (Object.keys(this.motionOptions.from).length > 0) {
                this.angularCropper.cropper.setData(Object.assign({}, this.motionOptions.from));
            }
        }
        else {
            this.angularCropper.cropper.clear();
            this.angularCropper.cropper.disable();
        }
    };
    ModalPhotoEditComponent.prototype.onPositionChange = function (value) {
        this.motionOptions[this.position] = this.angularCropper.cropper.getData(true);
        this.position = value;
        if (this.motionOptions[value]) {
            this.angularCropper.cropper.reset();
            this.angularCropper.cropper.setData(Object.assign({}, this.motionOptions[value]));
        }
    };
    ModalPhotoEditComponent.prototype.close = function (event) {
        event.preventDefault();
        this.bsModalRef.hide();
    };
    ModalPhotoEditComponent.prototype.save = function (event) {
        if (event) {
            event.preventDefault();
        }
        if (!this.media.options || Array.isArray(this.media.options)) {
            this.media.options = {};
        }
        if (!this.media.motionOptions || Array.isArray(this.media.motionOptions)) {
            this.media.motionOptions = { from: {}, to: {} };
        }
        if (this.angularCropper && this.angularCropper.cropper) {
            this.motionOptions[this.position] = this.angularCropper.cropper.getData(true);
        }
        Object.assign(this.media.options, this.options, { customized: this.isEditMode });
        Object.assign(this.media.motionOptions, this.motionOptions);
        this.bsModalRef.hide();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('imageElement'),
        __metadata("design:type", Object)
    ], ModalPhotoEditComponent.prototype, "imageElement", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('angularCropper'),
        __metadata("design:type", angular_cropperjs__WEBPACK_IMPORTED_MODULE_2__["CropperComponent"])
    ], ModalPhotoEditComponent.prototype, "angularCropper", void 0);
    ModalPhotoEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal-photo-edit',
            template: __webpack_require__(/*! ./modal-photo-edit.html */ "./src/app/project/modal-photo-edit.html")
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], ModalPhotoEditComponent);
    return ModalPhotoEditComponent;
}());



/***/ }),

/***/ "./src/app/project/modal-photo-edit.html":
/*!***********************************************!*\
  !*** ./src/app/project/modal-photo-edit.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">{{ 'EDIT' | translate }}</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"bsModalRef.hide()\"></button>\n</div>\n<div class=\"modal-body\">\n\n    <div class=\"card card-body mb-4 p-3\" *ngIf=\"isExtendMode\">\n        <div class=\"row\">\n            <div class=\"col-4\">\n                <label class=\"d-block\">{{ 'SLIDE_DURATION' | translate }}:</label>\n                <div>\n                    <select class=\"form-control form-control-sm\" [(ngModel)]=\"options.duration\">\n                        <option value=\"100\">4</option>\n                        <option value=\"150\">6</option>\n                        <option value=\"200\">8</option>\n                        <option value=\"250\">10</option>\n                        <option value=\"300\">12</option>\n                        <option value=\"350\">14</option>\n                        <option value=\"400\">16</option>\n                        <option value=\"450\">18</option>\n                        <option value=\"500\">20</option>\n                        <option value=\"625\">25</option>\n                        <option value=\"750\">30</option>\n                    </select>\n                </div>\n            </div>\n            <div class=\"col-4\">\n\n                <label class=\"d-block\">{{ 'OVERLAY_TEXT_POSITION' | translate }}:</label>\n                <div>\n                    <select class=\"form-control form-control-sm\" [(ngModel)]=\"options.text_position\">\n                        <option value=\"top_left\">{{ 'TOP_LEFT' | translate }}</option>\n                        <option value=\"top_center\">{{ 'TOP_CENTER' | translate }}</option>\n                        <option value=\"top_right\">{{ 'TOP_RIGHT' | translate }}</option>\n                        <option value=\"bottom_left\">{{ 'BOTTOM_LEFT' | translate }}</option>\n                        <option value=\"bottom_center\">{{ 'BOTTOM_CENTER' | translate }}</option>\n                        <option value=\"bottom_right\">{{ 'BOTTOM_RIGHT' | translate }}</option>\n                    </select>\n                </div>\n\n            </div>\n            <div class=\"col-4\">\n\n                <label class=\"d-block\">{{ 'SLIDE_TRANSITION' | translate }}:</label>\n                <div>\n                    <select class=\"form-control form-control-sm\" [(ngModel)]=\"options.transition\">\n                        <option value=\"fade\">{{ 'FADE' | translate }}</option>\n                        <option value=\"wipe__linear_x\">{{ 'WIPE_LEFT' | translate }}</option>\n                        <option value=\"wipe__clock\">{{ 'CLOCK' | translate }}</option>\n                        <option value=\"wipe__radial-bars\">{{ 'RADIAL_BARS' | translate }}</option>\n                        <option value=\"wipe__cloud\">{{ 'CLOUD' | translate }}</option>\n                        <option value=\"wipe__spiral\">{{ 'SPIRAL' | translate }}</option>\n                        <option value=\"wipe__checkerboard_small\">{{ 'CHECKERBOARD' | translate }}</option>\n                    </select>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n    <div class=\"card card-body mb-4 p-3\">\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <label class=\"custom-switch\">\n                    <input type=\"checkbox\" name=\"edit_mode\" value=\"1\" class=\"custom-switch-input\" [checked]=\"isEditMode\"\n                           [ngModel]=\"isEditMode\" (ngModelChange)=\"onModeChange($event)\">\n                    <span class=\"custom-switch-indicator\"></span>\n                    <span class=\"custom-switch-description\">\n                                {{ 'EDIT_MOTION' | translate }}\n                            </span>\n                </label>\n            </div>\n            <div class=\"col-6\">\n                <div *ngIf=\"isEditMode\">\n                    <div class=\"custom-control custom-radio custom-control-inline\">\n                        <input type=\"radio\" id=\"fieldPosition1\" name=\"position\" class=\"custom-control-input\"\n                               value=\"from\" [checked]=\"position == 'from'\" [ngModel]=\"position\"\n                               (ngModelChange)=\"onPositionChange($event)\">\n                        <label class=\"custom-control-label\" for=\"fieldPosition1\">\n                            {{ 'POSITION_FROM' | translate }}\n                        </label>\n                    </div>\n                    <div class=\"custom-control custom-radio custom-control-inline\">\n                        <input type=\"radio\" id=\"fieldPosition2\" name=\"position\" class=\"custom-control-input\" value=\"to\"\n                               [checked]=\"position == 'to'\" [ngModel]=\"position\"\n                               (ngModelChange)=\"onPositionChange($event)\">\n                        <label class=\"custom-control-label\" for=\"fieldPosition2\">\n                            {{ 'POSITION_TO' | translate }}\n                        </label>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <angular-cropper\n        [cropperOptions]=\"cropperOptions\"\n        [imageUrl]=\"imageUrl\"\n        (ready)=\"onCropperReady($event)\"\n        #angularCropper></angular-cropper>\n</div>\n<div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-primary\" (click)=\"save($event)\">{{ 'SAVE' | translate }}</button>\n    <button type=\"button\" class=\"btn btn-default\" (click)=\"close($event)\">{{ 'CLOSE' | translate }}</button>\n</div>\n"

/***/ }),

/***/ "./src/app/project/project.component.css":
/*!***********************************************!*\
  !*** ./src/app/project/project.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QvcHJvamVjdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/project/project.component.html":
/*!************************************************!*\
  !*** ./src/app/project/project.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"position-relative\">\n    <div class=\"position-absolute\" style=\"left: 0; top: -80px;\">\n        <a class=\"btn btn-secondary btn-icon btn-pill\" routerLink=\"/home\" [title]=\"'HOME_PAGE' | translate\">\n            <i class=\"fe fe-home\"></i>\n        </a>\n    </div>\n</div>\n\n<div class=\"dimmer\" [ngClass]=\"{'active': loading}\">\n    <div class=\"loader\"></div>\n    <div class=\"min-height150 dimmer-content\">\n        <div class=\"my-5\">\n\n            <ng-container [ngSwitch]=\"true\">\n                <ng-container *ngSwitchCase=\"project.status === 'new'\">\n\n                    <div dragula=\"mediaItems\" [(dragulaModel)]=\"items\">\n                        <div class=\"card rounded\" *ngFor=\"let item of items; index as index\" [ngClass]=\"{'opacity70': !item.isActive}\">\n                            <div class=\"card-body py-2 px-3\">\n\n                                <div class=\"row\">\n                                    <div class=\"col-6\">\n\n                                        <div class=\"row\">\n                                            <div class=\"col-2 text-muted py-5 drag-handle\">\n                                                <div class=\"font-middle\">\n                                                    <i class=\"d-block fe fe-more-horizontal\"></i>\n                                                    <i class=\"d-block fe fe-more-horizontal\" style=\"margin-top: -1.4rem;\"></i>\n                                                    <i class=\"d-block fe fe-more-horizontal\" style=\"margin-top: -1.4rem;\"></i>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-10\">\n                                                <div class=\"position-relative text-center max-width-100 mx-auto show-on-hover-parent\">\n                                                    <img [src]=\"getImageUrl(item)\" alt=\"\" class=\"rounded-circle max-width-100\">\n                                                    <div class=\"position-absolute w-100 h-100 bg-white-transp show-on-hover\" style=\"left: 0; top: 0;\">\n                                                        <button type=\"button\" class=\"btn btn-secondary btn-icon btn-pill btn-icon-center\" [title]=\"'EDIT' | translate\" (click)=\"openModalPhotoEditComponent(item, $event)\">\n                                                            <i class=\"fe fe-edit\"></i>\n                                                        </button>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n\n                                    </div>\n                                    <div class=\"col-6\">\n\n                                        <div class=\"row\">\n                                            <div class=\"col-8 pt-2\">\n                                                <label for=\"fieldTextContent{{index}}\">\n                                                    {{'SLIDE_TEXT' | translate}}:\n                                                </label>\n                                                <input type=\"text\" id=\"fieldTextContent{{index}}\" class=\"form-control\" [(ngModel)]=\"item.textTitle\" [disabled]=\"!item.isActive\">\n                                            </div>\n                                            <div class=\"col-4 text-right pt-5\">\n                                                <label class=\"custom-switch mt-4 pr-2 cursor-pointer\">\n                                                    <input type=\"checkbox\" value=\"1\" class=\"custom-switch-input\" [(ngModel)]=\"item.isActive\">\n                                                    <span class=\"custom-switch-indicator\"></span>\n                                                </label>\n                                            </div>\n                                        </div>\n\n                                    </div>\n                                </div>\n\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"mb-3\">\n                        <div class=\"text-center\">\n                            <input type=\"file\" class=\"d-none\" accept=\"audio/*\" #fileInput (change)=\"onFileSelect()\">\n                            <button type=\"button\" class=\"btn btn-secondary d-inline-block vertical-top mb-2 mr-2\" (click)=\"selectFile($event)\" [ngClass]=\"{'btn-loading': loadingAudio}\">\n                                <i class=\"fe fe-upload\"></i>\n                                {{'SELECT_AUDIO_FILE' | translate}}\n                            </button>\n                            <select class=\"form-control d-inline-block w-auto vertical-top mb-2 mr-2\" [(ngModel)]=\"project.options.musicName\" (ngModelChange)=\"this.audioPlayer.pause()\">\n                                <option *ngFor=\"let musicName of music\" [value]=\"musicName\">{{musicName}}</option>\n                            </select>\n                            <button type=\"button\" class=\"btn btn-success d-inline-block vertical-top mb-2\" [title]=\"'PLAY_PAUSE' | translate\" (click)=\"playAudio($event)\">\n                                <i class=\"fe fe-play\" *ngIf=\"this.audioPlayer.paused\"></i>\n                                <i class=\"fe fe-pause\" *ngIf=\"!this.audioPlayer.paused\"></i>\n                            </button>\n                        </div>\n                    </div>\n\n                    <div class=\"text-center\">\n                        <div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\">\n                            <label class=\"btn btn-secondary\" [ngClass]=\"{'active': project.options.size === 'hd'}\">\n                                <input type=\"radio\" name=\"fieldOptionssize\" value=\"hd\" autocomplete=\"off\" [(ngModel)]=\"project.options.size\">\n                                HD\n                            </label>\n                            <label class=\"btn btn-secondary\" [ngClass]=\"{'active': project.options.size === 'full_hd'}\">\n                                <input type=\"radio\" name=\"fieldOptionssize\" value=\"full_hd\" autocomplete=\"off\" [(ngModel)]=\"project.options.size\">\n                                Full HD\n                            </label>\n                        </div>\n\n                        <button type=\"button\" class=\"btn btn-pill btn-primary btn-lg ml-4\" (click)=\"createProject()\" [ngClass]=\"{'btn-loading': renderingStarted}\" [disabled]=\"renderingStarted\">\n                            {{'CREATE_VIDEO' | translate}}\n                        </button>\n                    </div>\n\n                </ng-container>\n                <ng-container *ngSwitchCase=\"project.status === 'pending' || project.status === 'processing'\">\n\n                    <h3 class=\"text-secondary text-center\">\n                        {{'PLEASE_WAIT' | translate}}...\n                    </h3>\n\n                    <div class=\"my-5\">\n                        <div class=\"progress rounded border\" style=\"height: 2rem;\">\n                            <div class=\"progress-bar bg-success progress-bar-striped progress-bar-animated\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 0%\" #progressBar></div>\n                        </div>\n                        <div class=\"position-relative\">\n                            <div class=\"position-absolute text-center w-100\" style=\"bottom: 1rem;\">\n                                <div class=\"btn-loading btn-secondary\"></div>\n                            </div>\n                        </div>\n                        <div class=\"text-center py-2\">\n                            {{progressMessage}}\n                        </div>\n                    </div>\n\n                </ng-container>\n                <ng-container *ngSwitchCase=\"project.status === 'completed'\">\n\n                    <div class=\"bg-dark\">\n                        <video class=\"d-block\" [src]=\"movieFileUrl\" controls style=\"width: 100%; height: 408px;\" #movieVideo></video>\n                    </div>\n\n                    <div class=\"py-5 text-center\">\n                        <a class=\"btn btn-primary btn-lg btn-pill\" href=\"/project_download/{{project.uniqueId}}\" target=\"_blank\">\n                            <i class=\"fe fe-download\"></i>\n                            {{'DOWNLOAD' | translate}}\n                        </a>\n                    </div>\n\n                </ng-container>\n            </ng-container>\n\n            <div class=\"alert alert-icon alert-danger mt-4\" role=\"alert\" *ngIf=\"errorMessage\">\n                <i class=\"fe fe-alert-triangle mr-2\" aria-hidden=\"true\"></i>\n                {{errorMessage}}\n            </div>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/project/project.component.ts":
/*!**********************************************!*\
  !*** ./src/app/project/project.component.ts ***!
  \**********************************************/
/*! exports provided: ProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectComponent", function() { return ProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_dragula__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-dragula */ "./node_modules/ng2-dragula/dist/fesm5/ng2-dragula.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _services_project_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/project.service */ "./src/app/services/project.service.ts");
/* harmony import */ var _models_project_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/project.model */ "./src/app/models/project.model.ts");
/* harmony import */ var _models_media_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/media.model */ "./src/app/models/media.model.ts");
/* harmony import */ var _modal_edit_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modal-edit.component */ "./src/app/project/modal-edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ProjectComponent = /** @class */ (function () {
    function ProjectComponent(router, dataService, route, dragulaService, modalService, translateService) {
        this.router = router;
        this.dataService = dataService;
        this.route = route;
        this.dragulaService = dragulaService;
        this.modalService = modalService;
        this.translateService = translateService;
        this.loading = false;
        this.renderingStarted = false;
        this.loadingAudio = false;
        this.errorMessage = '';
        this.project = new _models_project_model__WEBPACK_IMPORTED_MODULE_6__["Project"]('', '');
        this.items = [];
        this.progressMessage = '';
        this.music = [];
        this.audioPlayer = new Audio();
        this.musicUplodedFileUrl = '';
        this.movieFileUrl = '';
        this.dragulaService.destroy('mediaItems');
        dragulaService.createGroup('mediaItems', {
            moves: function (el, container, handle) {
                return handle.className.indexOf('drag-handle') > -1;
            }
        });
        this.project.options.size = 'hd';
        this.project.options.musicName = '';
        this.project.options.musicId = 0;
    }
    ProjectComponent.prototype.ngOnInit = function () {
        this.getProject();
        this.getMusic();
    };
    ProjectComponent.prototype.getLangString = function (value) {
        if (!this.translateService.store.translations[this.translateService.currentLang]) {
            return value;
        }
        var translations = this.translateService.store.translations[this.translateService.currentLang];
        return translations[value] || value;
    };
    ProjectComponent.prototype.getProject = function () {
        var _this = this;
        this.project.uniqueId = this.route.snapshot.paramMap.get('uniqueId');
        this.loading = true;
        this.errorMessage = '';
        this.dataService.getItem(this.project.uniqueId)
            .subscribe(function (res) {
            _this.project.status = res['status'];
            if (res['items']) {
                _this.items = res['items'];
            }
            // if (res['options'] && !Array.isArray(res['options'])) {
            //     this.project.options = res['options'];
            // }
            if (res['options'] && res['options']['musicId']) {
                _this.project.options.musicId = res['options']['musicId'];
            }
            if (res['options'] && res['options']['musicName']) {
                _this.project.options.musicName = res['options']['musicName'];
            }
            if (res['musicFileUrl']) {
                _this.musicUplodedFileUrl = res['musicFileUrl'];
                _this.music.push('Uploaded');
                _this.project.options.musicName = 'Uploaded';
            }
            if (res['movieUrl']) {
                _this.movieFileUrl = res['movieUrl'];
            }
            if (['pending', 'processing'].indexOf(_this.project.status) > -1) {
                _this.getProgress();
            }
            _this.loading = false;
            _this.onGetProject(res);
        }, function (err) {
            if (err['error']) {
                _this.errorMessage = err['error'];
            }
            _this.loading = false;
        });
    };
    ProjectComponent.prototype.onGetProject = function (responce) {
        if (window.parent && typeof window.parent['vssmExportUrl'] === 'function') {
            window.parent['vssmExportUrl'](responce);
        }
    };
    ProjectComponent.prototype.getMusic = function () {
        var _this = this;
        this.dataService.getMusic()
            .subscribe(function (res) {
            _this.music = res;
            if (!_this.project.options.musicName) {
                _this.project.options.musicName = _this.music[0];
            }
        }, function (err) {
            if (err['error']) {
                _this.errorMessage = err['error'];
            }
            _this.loading = false;
        });
    };
    ProjectComponent.prototype.openModalPhotoEditComponent = function (media, event) {
        if (event) {
            event.preventDefault();
        }
        var initialState = {
            media: media,
            project: this.project
        };
        this.bsModalRef = this.modalService.show(_modal_edit_component__WEBPACK_IMPORTED_MODULE_8__["ModalPhotoEditComponent"], {
            initialState: initialState,
            animated: false,
            ignoreBackdropClick: true,
            'class': 'modal-lg'
        });
    };
    ProjectComponent.prototype.getImageUrl = function (media) {
        return _models_media_model__WEBPACK_IMPORTED_MODULE_7__["Media"].getMediaResizedUrl(media, this.project.uniqueId);
    };
    ProjectComponent.prototype.getProgress = function () {
        var _this = this;
        clearTimeout(this.timer);
        this.timer = setTimeout(function () {
            _this.dataService.getProgress(_this.project.uniqueId)
                .subscribe(function (res) {
                if (res['queue_number']) {
                    _this.progressMessage = _this.getLangString('YOU_QUEUE_NUMBER') + ": " + res['queue_number'];
                }
                var percent = 0;
                if (res['percent']) {
                    percent = res['percent'];
                    _this.progressBar.nativeElement.style.width = percent + "%";
                    _this.progressMessage = percent + "%";
                }
                if (percent === 100) {
                    setTimeout(_this.getProject.bind(_this), 2000);
                }
                else {
                    _this.getProgress();
                }
            }, function (err) {
                if (err['error']) {
                    _this.errorMessage = err['error'];
                }
            });
        }, 3000);
    };
    ProjectComponent.prototype.playAudio = function (event) {
        if (event) {
            event.preventDefault();
        }
        if (!this.project.options.musicName) {
            return;
        }
        var audioUrl = this.project.options.musicName === 'Uploaded'
            ? this.musicUplodedFileUrl
            : "library/music/" + this.project.options.musicName;
        if (!this.audioPlayer.paused && decodeURIComponent(this.audioPlayer.src).indexOf(audioUrl) > -1) {
            this.audioPlayer.pause();
            return;
        }
        this.audioPlayer.src = audioUrl;
        this.audioPlayer.play();
    };
    ProjectComponent.prototype.selectFile = function (event) {
        if (event) {
            event.preventDefault();
        }
        this.fileInput.nativeElement.click();
    };
    ProjectComponent.prototype.onFileSelect = function () {
        var _this = this;
        var files = this.fileInput.nativeElement.files;
        var data = {
            type: 'music',
            file: files[0]
        };
        var url = this.dataService.getRequestFullUrl('upload', this.project.uniqueId);
        this.errorMessage = '';
        this.loadingAudio = true;
        this.dataService.postFormData(this.dataService.getFormData(data), url)
            .subscribe(function (res) {
            if (res['success']) {
                if (res['musicId']) {
                    _this.project.options.musicId = res['musicId'];
                    _this.project.options.musicName = 'Uploaded';
                    if (_this.music.indexOf('Uploaded') === -1) {
                        _this.music.push('Uploaded');
                    }
                    if (res['musicFileUrl']) {
                        _this.musicUplodedFileUrl = res['musicFileUrl'];
                    }
                }
            }
            _this.fileInput.nativeElement.files = null;
            _this.fileInput.nativeElement.value = null;
            _this.loadingAudio = false;
        }, function (err) {
            if (err['error']) {
                _this.errorMessage = err['error'];
            }
            _this.loadingAudio = false;
        });
    };
    ProjectComponent.prototype.createProject = function () {
        var _this = this;
        if (!this.audioPlayer.paused) {
            this.audioPlayer.pause();
        }
        if (this.renderingStarted) {
            return;
        }
        this.errorMessage = '';
        this.renderingStarted = true;
        this.dataService.saveProjectData(this.project.uniqueId, {
            options: this.project.options,
            items: this.items
        })
            .subscribe(function (res) {
            if (res['success']) {
                _this.project.status = 'pending';
            }
            _this.getProgress();
        }, function (err) {
            if (err['error']) {
                _this.errorMessage = err['error'];
            }
            _this.loading = false;
            _this.renderingStarted = false;
        });
    };
    ProjectComponent.prototype.ngOnDestroy = function () {
        if (!this.audioPlayer.paused) {
            this.audioPlayer.pause();
        }
        this.dragulaService.destroy('mediaItems');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('progressBar'),
        __metadata("design:type", Object)
    ], ProjectComponent.prototype, "progressBar", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", Object)
    ], ProjectComponent.prototype, "fileInput", void 0);
    ProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project',
            template: __webpack_require__(/*! ./project.component.html */ "./src/app/project/project.component.html"),
            styles: [__webpack_require__(/*! ./project.component.css */ "./src/app/project/project.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_project_service__WEBPACK_IMPORTED_MODULE_5__["ProjectService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            ng2_dragula__WEBPACK_IMPORTED_MODULE_2__["DragulaService"],
            ngx_bootstrap__WEBPACK_IMPORTED_MODULE_3__["BsModalService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]])
    ], ProjectComponent);
    return ProjectComponent;
}());



/***/ }),

/***/ "./src/app/services/data-service.abstract.ts":
/*!***************************************************!*\
  !*** ./src/app/services/data-service.abstract.ts ***!
  \***************************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");



var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        });
        this.requestUrl = '';
        this.requestUrl = 'app/data_list';
    }
    DataService.prototype.setRequestUrl = function (url) {
        this.requestUrl = url;
    };
    DataService.prototype.getRequestUrl = function () {
        return this.requestUrl;
    };
    DataService.prototype.getRequestFullUrl = function (action, id) {
        switch (action) {
            case 'get_one':
            case 'delete':
                return this.getRequestUrl() + ("/" + id);
            case 'update':
                return this.getRequestUrl() + ("/" + id + "/update");
            case 'upload':
                return this.getRequestUrl() + ("/" + id + "/upload");
            default:
                return this.getRequestUrl();
        }
    };
    DataService.prototype.getParams = function (options) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]();
        for (var name_1 in options) {
            if (!options.hasOwnProperty(name_1)
                || typeof options[name_1] === 'undefined') {
                continue;
            }
            params = params.append(name_1, options[name_1]);
        }
        return params;
    };
    DataService.prototype.getItem = function (id) {
        var url = this.getRequestUrl() + ("/" + id);
        return this.http.get(url, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.getList = function (options) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]();
        for (var name_2 in options) {
            if (!options.hasOwnProperty(name_2)
                || typeof options[name_2] === 'undefined') {
                continue;
            }
            params = params.append(name_2, options[name_2]);
        }
        return this.http.get(this.getRequestUrl(), { params: params, headers: this.headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.getListPage = function (options) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]();
        for (var name_3 in options) {
            if (!options.hasOwnProperty(name_3)
                || typeof options[name_3] === 'undefined') {
                continue;
            }
            params = params.append(name_3, options[name_3]);
        }
        return this.http.get(this.getRequestUrl(), { params: params, headers: this.headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.deleteItem = function (id) {
        var url = this.getRequestUrl() + ("/" + id);
        return this.http.delete(url, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.deleteByArray = function (idsArray) {
        var url = this.getRequestUrl() + '/batch';
        var data = { ids: idsArray };
        return this.http.post(url, data, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.create = function (item) {
        return this.http.post(this.getRequestUrl(), item, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.update = function (item) {
        var url = this.getRequestUrl() + ("/" + item.id);
        return this.http.put(url, item, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.updateProperty = function (itemId, optionsName, value) {
        var url = this.getRequestUrl() + ("/update/" + optionsName + "/" + itemId);
        return this.http.patch(url, { value: value }, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.getFormData = function (item) {
        var formData = new FormData();
        Object.keys(item).forEach(function (key) {
            if (item[key] instanceof File) {
                formData.append(key, item[key], item[key].name);
            }
            else if (typeof item[key] !== 'undefined') {
                if (typeof item[key] === 'boolean') {
                    formData.append(key, item[key] ? '1' : '0');
                }
                else {
                    formData.append(key, item[key] || '');
                }
            }
        });
        return formData;
    };
    DataService.prototype.postFormData = function (formData, url) {
        url = url || this.getRequestFullUrl();
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
            'enctype': 'multipart/form-data',
            'Accept': 'application/json'
        });
        return this.http
            .post(url, formData, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    DataService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (err) {
            if (err.error) {
                throw err.error;
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(result);
        };
    };
    return DataService;
}());



/***/ }),

/***/ "./src/app/services/media.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/media.service.ts ***!
  \*******************************************/
/*! exports provided: MediaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaService", function() { return MediaService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _data_service_abstract__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data-service.abstract */ "./src/app/services/data-service.abstract.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MediaService = /** @class */ (function (_super) {
    __extends(MediaService, _super);
    function MediaService(http) {
        var _this = _super.call(this, http) || this;
        _this.setRequestUrl('api/media');
        return _this;
    }
    MediaService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], MediaService);
    return MediaService;
}(_data_service_abstract__WEBPACK_IMPORTED_MODULE_2__["DataService"]));



/***/ }),

/***/ "./src/app/services/project.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/project.service.ts ***!
  \*********************************************/
/*! exports provided: ProjectService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectService", function() { return ProjectService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _data_service_abstract__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./data-service.abstract */ "./src/app/services/data-service.abstract.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProjectService = /** @class */ (function (_super) {
    __extends(ProjectService, _super);
    function ProjectService(http) {
        var _this = _super.call(this, http) || this;
        _this.setRequestUrl('api/project');
        return _this;
    }
    ProjectService.prototype.saveProjectData = function (projectUniqueId, mediaItems) {
        var url = this.getRequestUrl() + ("/" + projectUniqueId);
        return this.http.post(url, mediaItems, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    ProjectService.prototype.getProgress = function (projectUniqueId) {
        var url = "api/project_progress/" + projectUniqueId;
        return this.http.get(url, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    ProjectService.prototype.getMusic = function () {
        var url = 'api/library_music';
        return this.http.get(url, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError()));
    };
    ProjectService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProjectService);
    return ProjectService;
}(_data_service_abstract__WEBPACK_IMPORTED_MODULE_3__["DataService"]));



/***/ }),

/***/ "./src/app/services/translateLoader.ts":
/*!*********************************************!*\
  !*** ./src/app/services/translateLoader.ts ***!
  \*********************************************/
/*! exports provided: TranslateCustomLoader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TranslateCustomLoader", function() { return TranslateCustomLoader; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

var TranslateCustomLoader = /** @class */ (function () {
    function TranslateCustomLoader() {
    }
    TranslateCustomLoader.prototype.getTranslation = function (lang) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(window['APP_LANG']);
    };
    return TranslateCustomLoader;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp64\www\videomaker\frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map