<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        /*$userDemo = new User();
        $userDemo
            ->setEmail('demo')
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $userDemo->setPassword($this->passwordEncoder->encodePassword(
            $userDemo,
            'demo'
        ));
        $manager->persist($userDemo);
        $manager->flush();*/

        $admin = new User();
        $admin
            ->setEmail('admin')
            ->setRoles(['ROLE_USER', 'ROLE_SUPER_ADMIN']);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'admin'
        ));
        $manager->persist($admin);
        $manager->flush();
    }
}
