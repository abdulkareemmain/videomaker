<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class UtilsService
{
    /** @var ContainerInterface */
    protected $container;

    /** @param ContainerInterface $container */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Send mail
     * @param $subject
     * @param $mailBody
     * @param $toEmail
     * @return bool|int
     */
    public function sendMail($subject, $mailBody, $toEmail)
    {
        if (empty($this->container->getParameter('app.admin_email'))) {
            return false;
        }
        /** @var \Swift_Mailer $mailer */
        $mailer = $this->container->get('mailer');
        $message = (new \Swift_Message($subject))
            ->setFrom(
                $this->container->getParameter('app.admin_email'),
                $this->container->getParameter('app.name')
            )
            ->setTo($toEmail)
            ->setReplyTo(
                $this->container->getParameter('app.admin_email')
            )
            ->setBody(
                $mailBody,
                'text/html'
            );

        return $mailer->send($message);
    }

    /**
     * @param int $length
     * @param array $options
     * @return string
     */
    public static function generatePassword($length = 12, array $options = array())
    {
        $options = array_merge(array(
            'allowable_characters' => 'abcdefghjkmnpqrstuvxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789',
            'srand_seed_multiplier' => 1000000,
        ), $options);
        $ps_len = strlen($options['allowable_characters']);
        srand((double) microtime() * $options['srand_seed_multiplier']);
        $pass = '';
        for ($i = 0; $i < $length; $i++) {
            $pass .= $options['allowable_characters'][mt_rand(0, $ps_len -1)];
        }
        return $pass;
    }

    /**
     * @param $keyArray
     * @param $valueArray
     * @return array
     */
    public static function arrayFillKeys($keyArray, $valueArray) {
        $filledArray = [];
        if(is_array($keyArray)) {
            foreach($keyArray as $key => $value) {
                $filledArray[$value] = $valueArray[$key];
            }
        }
        return $filledArray;
    }

    public static function get_timezone_offset($remote_tz, $origin_tz = null) {
        if($origin_tz === null) {
            if(!is_string($origin_tz = date_default_timezone_get())) {
                return false;
            }
        }
        $origin_dtz = new \DateTimeZone($origin_tz);
        $remote_dtz = new \DateTimeZone($remote_tz);
        $origin_dt = new \DateTime('now', $origin_dtz);
        $remote_dt = new \DateTime('now', $remote_dtz);
        $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
        return $offset / 60 / 60;
    }

    /**
     * @param $dirPath
     */
    public static function cleanDirectory($dirPath)
    {
        if (!is_dir($dirPath)) {
            return;
        }
        $dir = new \DirectoryIterator($dirPath);
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                if (is_file($fileinfo->getPathname())) {
                    unlink($fileinfo->getPathname());
                }
            }
        }
    }

    /**
     * @param $str
     * @param int $userId
     * @return bool
     */
    public static function logging($str, $userId = 0)
    {
        if (is_array($str)) {
            $str = json_encode($str);
        }
        $rootPath = dirname(dirname(__DIR__));
        if( $userId ){
            $logFilePath = $rootPath . '/' . $userId . '_user_log.txt';
        } else {
            $logFilePath = $rootPath . '/log.txt';
        }

        $options = [
            'max_log_size' => 200 * 1024
        ];

        if (!is_dir(dirname($logFilePath))) {
            mkdir(dirname($logFilePath));
            chmod(dirname($logFilePath), 0777);
        }
        if (file_exists($logFilePath) && filesize($logFilePath) >= $options['max_log_size']) {
            @unlink($logFilePath);
        }

        $fp = fopen( $logFilePath, 'a' );
        $dateFormat = 'd/m/Y H:i:s';
        $str = PHP_EOL . PHP_EOL . date($dateFormat) . PHP_EOL . $str;

        fwrite( $fp, $str );
        fclose( $fp );
        @chmod( $logFilePath, 0777 );

        return true;
    }

    /**
     * @param array $queryOptions
     * @param int $itemsTotal
     * @param array $catalogNavSettingsDefaults
     * @param array $options
     * @return array
     * @internal param array $pageSizeArr
     */
    public static function getPagesOptions($queryOptions, $itemsTotal, $catalogNavSettingsDefaults = [], $options = [])
    {
        $pagesOptions = [
            'pageSizeArr' => isset($catalogNavSettingsDefaults['pageSizeArr'])
                ? $catalogNavSettingsDefaults['pageSizeArr']
                : [12],
            'current' => $queryOptions['page'],
            'limit' => $queryOptions['limit'],
            'total' => ceil($itemsTotal / $queryOptions['limit']),
            'prev' => max(1, $queryOptions['page'] - 1),
            'skip' => ($queryOptions['page'] - 1) * $queryOptions['limit'],
            'pageVar' => isset($options['pageVar']) ? $options['pageVar'] : 'page',
            'limitVar' => isset($options['limitVar']) ? $options['limitVar'] : 'limit',
            'orderByVar' => isset($options['orderByVar']) ? $options['orderByVar'] : 'order_by'
        ];
        $pagesOptions['next'] = min($pagesOptions['total'], $queryOptions['page'] + 1);
        return $pagesOptions;
    }
}
