<?php

namespace App\Controller\Admin;

use App\Entity\Media;
use App\Entity\Project;
use App\Service\UtilsService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/")
 * Class DefaultController
 * @package App\Controller\Admin
 */
class DefaultController extends AbstractController {

    /**
     * @Route("{page}", name="admin_homepage", requirements={"page": "\d+"}, defaults={"page": "1"})
     * @param Request $request
     * @param int|string $page
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function homeAction(Request $request, $page, EntityManagerInterface $em)
    {
        $limit = 20;
        $page = (int) $page;
        $itemsTotal = $em->getRepository(Project::class)->getCount();
        $pagesOptions = UtilsService::getPagesOptions(['page' => $page, 'limit' => $limit], $itemsTotal);

        $projects = $em->getRepository(Project::class)
            ->findBy([], ['createdAt' => 'asc'], $limit, $pagesOptions['skip']);

        return $this->render('admin/homepage.html.twig', [
            'projects' => $projects,
            'pagesOptions' => $pagesOptions
        ]);
    }

    /**
     * @Route("delete/{id}", name="admin_delete_project", requirements={"id": "\d+"})
     * @IsGranted("ROLE_ADMIN_WRITE", statusCode="400", message="Your user has read-only permission.")
     * @ParamConverter("post", class="App:Project")
     * @param Request $request
     * @param Project $project
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function deleteProjectAction(Request $request, Project $project, EntityManagerInterface $em)
    {
        if ($project->getStatus() == Project::STATUS_PROCESSING) {
            $this->addFlash('message', 'You cannot delete a project that has started to be rendered.');
            return $this->redirectToRoute('admin_homepage');
        }
        $movieMedia = $project->getMovie();
        $mediaList = $em->getRepository(Media::class)->findProjectMedia($project->getId());

        /** @var Media $media */
        foreach ($mediaList as $media) {
            if ($movieMedia && $movieMedia->getId() === $media->getId()) {
                $project->setMovie(null);
            }
            $em->remove($media);
            $em->flush();
        }
        $em->remove($project);
        $em->flush();

        return $this->redirectToRoute('admin_homepage');
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return realpath($this->getParameter('app.project_dir_path'));
    }
}
